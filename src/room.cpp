#include "room.hpp"

Room::Room(int x, int y, int width, int length):_width(width),_length(length){
    _position.x = x;
    _position.y = y;
    buildRoom();
}

void Room::buildRoom(){
    SDL_Rect rect_width1 = {_position.x, _position.y, 1, 1};
    SDL_Rect rect_width2 = {_position.x, _position.y+_length+1, 1, 1};
    SDL_Rect rect_length1 = {_position.x, _position.y+1, 1, 1};
    SDL_Rect rect_length2 = {_position.x+_width+1, _position.y+1, 1, 1};

    rect_width1.x += 1;
    rect_width2.x += 1;

    SDL_Color color = {255, 255, 255, 255};
    // draw edges;
    for(int i=0;i<_width;i++){
        Solid solid("-");
        solid.SetPosition(rect_width1.x, rect_width1.y);
        solid.SetColor(color);
        _walls.push_back(solid);
        solid.SetPosition(rect_width2.x, rect_width2.y);
        solid.SetColor(color);
        _walls.push_back(solid);
        rect_width1.x += 1;
        rect_width2.x += 1;
    }
    for(int i=0;i<_length;++i){
        Solid solid("|");
        solid.SetPosition(rect_length1.x, rect_length1.y);
        solid.SetColor(color);
        _walls.push_back(solid);
        solid.SetPosition(rect_length2.x, rect_length2.y);
        solid.SetColor(color);
        _walls.push_back(solid);
        rect_length1.y += 1;
        rect_length2.y += 1;
    }

    // draw corner '+'
    Solid solid("+");
    solid.SetPosition(_position.x, _position.y);
    solid.SetColor(color);
    _walls.push_back(solid);
    solid.SetPosition(_position.x+_width+1, rect_width1.y);
    solid.SetColor(color);
    _walls.push_back(solid);
    solid.SetPosition(_position.x, _position.y+_length+1);
    solid.SetColor(color);
    _walls.push_back(solid);
    solid.SetPosition(_position.x+_width+1, _position.y+_length+1);
    solid.SetColor(color);
    _walls.push_back(solid);

}

void Room::Draw(){
    for(Solid& s : _walls)
        s.Draw();
}