#include "curse_sheet.hpp"

CurseSheet::CurseSheet(){
    _sheet = new ImageSheet("resources/curses_square_16x16.bmp", 16, 16, {251, 0, 255, 255});
}

SDL_Texture* CurseSheet::Get(string c){
    unsigned int idx = curse_char2idx.at(c);
    return _sheet->Get(idx%16, idx/16);
}

int CurseSheet::Col() const{
    return 13;
}

int CurseSheet::Row() const{
    return 16;
}

int CurseSheet::Width() const{
    return 16;
}

int CurseSheet::Height() const{
    return 16;
}

CurseSheet::~CurseSheet(){
    delete _sheet;
}

// 全局的字符图像集，只用一次就可以了
CurseSheet* charset = nullptr;

/**
 * @brief 绘制Curse Sheet内的字符,可以指定颜色
 * @param 
 *  curse 字符的名称
 *  x   字符按照网格的x坐标（不是像素
 *  y   字符按照网格的y坐标（不是像素
 */
void DrawCurse(string curse, int x, int y, Uint8 r, Uint8 g, Uint8 b){
    SDL_Texture* tex = charset->Get(curse);
    SDL_SetTextureColorMod(tex, r, g, b);
    SDL_Rect rect = {x*CURSE_WIDTH, y*CURSE_HEIGHT, CURSE_WIDTH, CURSE_HEIGHT};
    SDL_RenderCopy(render, tex, nullptr, &rect);
}

void DrawCurse(string curse, int x, int y, SDL_Color color){
    DrawCurse(curse, x, y, color.r, color.g, color.b);
}
