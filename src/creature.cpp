#include "creature.hpp"

const char* creature_name[] = {
    "Player",       // 玩家
    "wood_target",  // 用于demo的靶子  
    "Bugs"          // 虫子
};

void CreatureProperties::Set(int hp, int mp, int att, int def, int hungry, int speed, int strength, int dex, int intelli, int power, float burden){
    hp_ = hp;
    mp_ = mp;
    strength_ = strength;
    attack_ = att;
    defence_ = def;
    hungry_ = hungry;
    speed_ = speed;
    dex_ = dex;
    burden_ = burden;
    power_ = power;
    intelli_ = intelli;
}

Creature::Creature(CreatureName name){
    _name = name;
}

const char * Creature::GetName(){
    return creature_name[static_cast<int>(_name)];
}

void Creature::Draw(){
    string c;
    switch(_name){
        case CreatureName::PLAYER:
            c = "@";
            break;
        case CreatureName::WOOD_TARGET:
            c = CURSE_YUAN;
            break;
    }
    DrawCurse(c, _position.x, _position.y, _color);
}

Collidable* Creature::GetCollidable(){
    return _collide;
}
