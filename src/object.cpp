#include "object.hpp"

SDL_Point Object::GetPosition() const{
    return _position;
}

void Object::SetPosition(int x, int y){
    _position.x = x;
    _position.y = y;
}

void Object::SetColor(SDL_Color color) {
    _color = color;
}

SDL_Color Object::GetColor(){
    return _color;
}