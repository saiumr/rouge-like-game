#include "text_editor.hpp"

TTF_Font* font = nullptr;

void InitTextResources(){
    font = TTF_OpenFont("resources/SimHei.ttf", FONT_SIZE);
}

void DrawText(string text, int x, int y, SDL_Color color){
    color.a = 255;
    SDL_Surface* sur = TTF_RenderUTF8_Solid(font, text.c_str(), color);
    SDL_Texture* tex = SDL_CreateTextureFromSurface(render, sur);
    SDL_Rect rect = {x*CURSE_WIDTH, y*CURSE_HEIGHT, sur->w, sur->h};
    SDL_RenderCopy(render, tex, nullptr, &rect);
    SDL_FreeSurface(sur);
}

void ReleaseTextResources(){
    TTF_CloseFont(font);
}