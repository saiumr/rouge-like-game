#include "controller.hpp"

GamingController GamingController::_instance;

GamingController& GamingController::GetInstance(){
    return _instance;
}

void GamingController::Control(SDL_Event &event, Creature &player)
{
    if (event.type == SDL_KEYDOWN)
    {
        if (event.key.keysym.sym == SDLK_F1)
        {
            Exit();
        }
        else if (event.key.keysym.sym == SDLK_UP)
        {
            SDL_Point pos = player.GetPosition();
            player.SetPosition(pos.x, pos.y - 1);
        }
        else if (event.key.keysym.sym == SDLK_DOWN)
        {
            SDL_Point pos = player.GetPosition();
            player.SetPosition(pos.x, pos.y + 1);
        }
        else if (event.key.keysym.sym == SDLK_LEFT)
        {
            SDL_Point pos = player.GetPosition();
            player.SetPosition(pos.x - 1, pos.y);
        }
        else if (event.key.keysym.sym == SDLK_RIGHT)
        {
            SDL_Point pos = player.GetPosition();
            player.SetPosition(pos.x + 1, pos.y);
        }
    }
}