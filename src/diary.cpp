#include "diary.hpp"

void Diary::AddText(string text){
    _texts.push_back(move(text));
}

vector<string>& Diary::GetTexts(){
    return _texts;
}

void Diary::Draw(int x, int y, SDL_Color color){
    if(!_texts.empty())
        DrawText(_texts[_texts.size()-1], x, y, color);
}

Diary diary;