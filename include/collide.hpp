#ifndef COLLIDE_HPP
#define COLLIDE_HPP
#include "creature.hpp"
#include "diary.hpp"

class Creature;
class Solid;

class Collidable{
public:
    virtual bool Collide(Creature& me, Creature& other) = 0;
};

#endif

