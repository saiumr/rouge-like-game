#ifndef GAMEBODY_HPP
#define GAMEBODY_HPP
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "curse_sheet.hpp"
#include "room.hpp"
#include "controller.hpp"
#include "diary.hpp"
#include <string>
#include <iostream>
#include <cassert>
using namespace std;

class GameBody{
public:
    GameBody(string title, int width, int height);
    /*
     * @param 运行游戏，需要在main函数中调用一次
     * @warn 仅调用一次
     */
    void Run();
    ~GameBody();
private:
    /*
     * @brief 程序处理事件的函数
     */
    void eventHandle();
    /*
     * @brief 程序的主体框架
     */
    void step();

    SDL_Event _event;
    Controller* _controller;
};

#endif

