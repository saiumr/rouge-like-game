#ifndef CREATURE_HPP
#define CREATURE_HPP
#include "object.hpp"
#include "collide.hpp"
#include "curse_sheet.hpp"
#include <string>
using namespace std;

class Collidable;

enum class CreatureName{
    PLAYER = 0,
    WOOD_TARGET,
    BUGS
};

/* 
 * @brief 生物的各种属性
 */
struct CreatureProperties{
    int hp_;    /** 生命值 */
    int mp_;    /** 魔力值 */
    int attack_;    /** 攻击程度（命中率）*/
    int defence_;   /** 防御力 */
    int hungry_;    /** 饥饿度 */
    int speed_;     /** 速度 */
    
    //下面是四维
    int strength_;  /** 力量 */
    int dex_;       /** 敏捷 */
    int intelli_;   /** 智力 */
    int power_;     /** 体力 */
    float burden_;   /** 负重 */
    /*
     * @brief 设置所有属性的函数
     */
    void Set(int hp, int mp, int att, int def, int hungry, int speed, int strength, int dex, int intelli, int power, float burden);
};

/*
 * @brief 所有生物都需要集成的类
 */
class Creature : public Object{
public:
    Creature(CreatureName name);
    Collidable* GetCollidable();
    const char* GetName();
    CreatureProperties properties_;
    void Draw();

private:
    Collidable* _collide;
    CreatureName _name;
};

#endif