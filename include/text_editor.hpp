#ifndef TEXT_EDITOR_HPP
#define TEXT_EDITOR_HPP
#include "SDL_ttf.h"
#include "sdl_resources.hpp"
#include <iostream>
using namespace std;
#define FONT_SIZE 16

void InitTextResources();
void DrawText(string text, int x, int y, SDL_Color color);
void ReleaseTextResources();

#endif