#ifndef SOLID_HPP
#define SOLID_HPP
#include "object.hpp"
#include "collide.hpp"
#include <string>
using namespace std;

class Solid : public Object{
public:
    Solid(string name);
    void Draw();
    void Update(Creature &creature);
private:
    Collidable *_colliable;
    string _name;
};

#endif