#ifndef OBJECT_HPP
#define OBJECT_HPP
#include "SDL.h"
#include "sdl_resources.hpp"

class Object{
public:
    SDL_Point GetPosition() const;
    void SetPosition(int x, int y);
    void SetColor(SDL_Color color);
    SDL_Color GetColor();
protected:
    SDL_Color _color;
    SDL_Point _position;
};

#endif
