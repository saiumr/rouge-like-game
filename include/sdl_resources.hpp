#ifndef SDL_RESOURCES_HPP
#define SDL_RESOURCES_HPP
#include "SDL.h"

extern SDL_Window* window;
extern SDL_Renderer* render;
extern SDL_Texture* canva;
extern bool isquit;

#define GRID_COL 80
#define GRID_ROW 40

#define CURSE_WIDTH 16
#define CURSE_HEIGHT 16

#define CANVA_WIDTH (GRID_COL*CURSE_WIDTH)
#define CANVA_HEIGHT (GRID_ROW*CURSE_HEIGHT)

void Exit();

#endif

