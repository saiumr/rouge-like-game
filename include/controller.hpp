#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP
#include "creature.hpp"

class Controller{
public:
    virtual void Control(SDL_Event& event, Creature& player) = 0;
};

class GamingController : public Controller{
public:
    static GamingController& GetInstance();
    void Control(SDL_Event& event, Creature& player) override;
private:
    static GamingController _instance;
};

#endif