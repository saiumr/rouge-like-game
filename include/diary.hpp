#ifndef DIARY_HPP
#define DIARY_HPP
#include "text_editor.hpp"
#include <string>
#include <vector>
using namespace std;

class Diary{
public:
    Diary() = default;
    void AddText(string text);
    vector<string>& GetTexts();
    void Draw(int x, int y, SDL_Color color);
private:
    vector<string> _texts;
};

extern Diary diary;

#endif