#ifndef CHARSET_HPP
#define CHARSET_HPP
#include "SDL.h"
#include "SDL_image.h"
#include "sdl_resources.hpp"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class ImageSheet{
public:
    ImageSheet(string filename, int row, int col, SDL_Color keycolor);
    SDL_Texture* Get(int col, int row);
    int Col() const;
    int Row() const;
    int Width() const;
    int Height() const;
    ~ImageSheet();
protected:
    int _col;
    int _row;
    int _width;
    int _height;
    vector<SDL_Texture*> _tex_list;
};

#endif

