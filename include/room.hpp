#ifndef ROOM_HPP
#define ROOM_HPP
#include "curse_sheet.hpp"
#include "solid.hpp"
using namespace std;

class Room : public Object{
public:
    Room(int x, int y, int width, int length);
    void Draw();
private:
    void buildRoom();

    vector<Solid> _walls;
    int _width;
    int _length;
};

#endif
