/*
 * @file curse_sheet.hpp
 * @brief 存储着所有字符图片的字符集类
 */
#ifndef CURSE_SHEET_HPP
#define CURSE_SHEET_HPP
#include "imagesheet.hpp"
#include <map>
using namespace std;

//charset in image sheet, the image please reference ![image](resources/curses_square_16x16.bmp)

//some special char in curse sheet
#define CURSE_HEART "heart"               // ♥︎
#define CURSE_RHOMBUS "rhombus"           // ♦︎
#define CURSE_CLUBS "clubs"               // ♣︎
#define CURSE_SPADE "spade"               // ♠︎
#define CURSE_DOT "dot"
#define CURSE_INV_DOT "inv-dot"
#define CURSE_CIRCLE "circle"
#define CURSE_INV_CIRCLE "inv-circle"
#define CURSE_MALE "male"                 // ♂︎
#define CURSE_FEMALE "female"             // ♀︎
#define CURSE_SINGLE_NOTE "single-note"   // ♪
#define CURSE_DOUBLE_NOTE "double-note"   // ♫
#define CURSE_SUM "sun"                   // ☼
#define CURSE_RIGHT "right"               // ▶︎
#define CURSE_LEFT "left"                 // ◀︎
#define CURSE_UP "up"                     // ▲
#define CURSE_DOWN "down"                 // ▼
#define CURSE_UPDOWN_ARROW "up-down-arrow"// ⬍
#define CURSE_PILCROW "pilcrow"           // ¶
#define CURSE_SECTION "section"           // §
#define CURSE_BOLD_UNDERLINE "bold-underline" // _
#define CURSE_UPDOWN_BASE_ARROW "up-down-base-arrow" //↨
#define CURSE_UP_ARROW "up-arrow"         // ↑
#define CURSE_DOWN_ARROW "down-arrow"     // ↓
#define CURSE_RIGHT_ARROW "right-arrow"   // →
#define CURSE_LEFT_ARROT "left-arrow"     // ←
#define CURSE_POT "pot"
#define CURSE_PARAGRAPH "paragraph"       // ₤
#define CURSE_YUAN "yuan"                 // ￥
#define CURSE_INV_QUESTION "inv_?"        // ¿
#define CURSE_CORNER_TL "corner_tl"       // 
#define CURSE_CORNER_TR "corner_tr"       // ¬
#define CURSE_1DIV2 "1/2"
#define CURSE_1DIV4 "1/4"
#define CURSE_INV_EXCLAIM "inv_!"
#define CURSE_INV_SCHEMA "<<"
#define CURSE_SCHEMA ">>"                 // ⨠
#define CURSE_WATER1 "water1"
#define CURSE_WATER2 "water2"
#define CURSE_WATER3 "water3"
#define CURSE_WALL_VERTICAL "wall_ver"
#define CURSE_DELTA "delta"

static const map<string, unsigned int> curse_char2idx({
        {"heart",   3},
        {"rhombus", 4},
        {"clubs",   5},
        {"spade",   6},
        {"dot",     7}, 
        {"inv_dot", 8},
        {"circle",  9}, 
        {"inv-circle", 10},
        {"male",    11},
        {"female",  12},
        {"single-note", 13},
        {"double-note", 14},
        {"sun",     15},
        {"right",   16},
        {"left",    17},
        {"up-down-arrow", 18},
        {"!!",      19},
        {"pilcrow", 20},
        {"section", 21},
        {"bold-underline", 22},
        {"up-down-base-arrow", 23},
        {"up-arrow", 24},
        {"down-arrow", 25},
        {"right-arrow", 26},
        {"left-arrow", 27},
        {"pot", 28},
        {"left-right-arrow", 29},
        {"up", 30},
        {"down", 31},

        {"!", 33},
        {"\"", 34},
        {"#", 35},
        {"$", 36},
        {"%", 37},
        {"&", 38},
        {"'", 39},
        {"(", 40},
        {")", 41},
        {"*", 42},
        {"+", 43},
        {",", 44},
        {"-", 45},
        {"_", 46},
        {"/", 47},
        {"0", 48},
        {"1", 49},
        {"2", 50},
        {"3", 51},
        {"4", 52},
        {"5", 53},
        {"6", 54},
        {"7", 55},
        {"8", 56},
        {"9", 57},
        {":", 58},
        {";", 59},
        {"<", 60},
        {"=", 61},
        {">", 62},
        {"?", 63},
        {"@", 64},

        {"A", 65},
        {"B", 66},
        {"C", 67},
        {"D", 68},
        {"E", 69},
        {"F", 70},
        {"G", 71},
        {"H", 72},
        {"I", 73},
        {"J", 74},
        {"K", 75},
        {"L", 76},
        {"M", 77},
        {"N", 78},
        {"O", 79},
        {"P", 80},
        {"Q", 81},
        {"R", 82},
        {"S", 83},
        {"T", 84},
        {"U", 85},
        {"V", 86},
        {"W", 87},
        {"X", 88},
        {"Y", 89},
        {"Z", 90},

        {"[",   91},
        {"\\",  92},
        {"]",   93},
        {"^",   94},
        {"__",  95},    // long underline
        {"`",   96},

        {"a", 97},
        {"b", 98},
        {"c", 99},
        {"d", 100},
        {"e", 101},
        {"f", 102},
        {"g", 103},
        {"h", 104},
        {"i", 105},
        {"j", 106},
        {"k", 107},
        {"l", 108},
        {"m", 109},
        {"n", 110},
        {"o", 111},
        {"p", 112},
        {"q", 113},
        {"r", 114},
        {"s", 115},
        {"t", 116},
        {"u", 117},
        {"v", 118},
        {"w", 119},
        {"x", 120},
        {"y", 121},
        {"z", 122},
        
        {"{", 123},
        {"|", 124},
        {"}", 125},
        {"~", 126},

        {"delta", 127}, 
        {"bigC", 128},
        {"u:", 129},
        {"e'", 130},
        {"a^", 131},
        {"a:", 132},
        {"a`", 133},
        {"ao", 134},
        {"smallC", 135},
        {"e^", 136},
        {"e:", 137},
        {"e`", 138},
        {"i:", 139},
        {"i^", 140},
        {"i`", 141},
        {"A:", 142},
        {"Ao", 143},
        {"E'", 144},
        {"ae", 145},
        {"AE", 146},
        {"o^", 147},
        {"o:", 148},
        {"o`", 149},
        {"u^", 150},
        {"u`", 151},
        {"y:", 152},
        {"O:", 153},
        {"U:", 154},
        {"c1", 155},
        {"paragraph", 156},
        {"yuan", 157},
        {"R*", 158},
        {"f_", 159},
        {"a'", 160},
        {"i'", 161},
        {"o'", 162},
        {"u'", 163},
        {"n~", 164},
        {"N~", 165},
        {"a_", 166},
        {"o_", 167},
        {"inv_?", 168},
        {"corner_tl", 169},
        {"corner_tr", 170},

        {"1/2", 171},
        {"1/4", 172},
        {"inv_!", 173},
        {"<<", 174},
        {">>", 175},
        {"water1", 176},
        {"water2", 177},
        {"water3", 178},
        {"wall_ver", 179}
        
        //TODO have some other special char not code

    });

class CurseSheet{
public:
    CurseSheet();
    SDL_Texture* Get(string c);
    int Col() const;
    int Row() const;
    int Width() const;
    int Height() const;
    ~CurseSheet();
private:
    ImageSheet* _sheet;
};

extern CurseSheet* charset;

void DrawCurse(string curse, int x, int y, Uint8 r, Uint8 g, Uint8 b);
void DrawCurse(string curse, int x, int y, SDL_Color color);

#endif

