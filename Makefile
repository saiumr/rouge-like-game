CXX = g++
STD = -std=c++11
SDL_HEADER = `sdl2-config --cflags` `pkg-config --cflags SDL2_image SDL2_ttf`
SDL_LIBS = `sdl2-config --libs` `pkg-config --libs SDL2_image SDL2_ttf`
DEBUG = 
HEADERS = $(wildcard *.hpp, include/*.hpp)
SRCS = ${wildcard *.cpp, src/*.cpp} main.cpp
OBJS = $(patsubst %.cpp, %.o, ${SRCS})
BINS = game

game:${OBJS} ${HEADERS}
	${CXX} ${OBJS} -o $@ -Iinclude ${SDL_HEADER} ${SDL_LIBS} ${DEBUG} ${STD}

${OBJS}:%.o:%.cpp
	${CXX} $< -c -o $@ -Iinclude ${SDL_HEADER} ${DEBUG} ${STD}

.PHONY:clean echo redo
clean:
	-rm *.o
	-rm -rf *.dSYM
	-rm ${BINS}
	-rm src/*.o
	-rm -rf src/*.dSYM
redo:
	make clean
	make
